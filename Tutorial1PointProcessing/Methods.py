from matplotlib import pyplot as plt
import numpy as np
import cv2

# Exercise 1
def get4Segments(img):
    # Getting the width and height of the img using .shape
    height = img.shape[0]
    width = img.shape[1]

    halfHeight = round(height / 2)
    halfWidth = round(width / 2)

    # Storing each segment of the image in a variable, taking into consideration that coordinates are y,x instead of x,y
    # Each segment is half of the height and width
    segment1 = img[0:halfHeight, 0:halfWidth]
    segment2 = img[0:halfHeight, halfWidth:width]
    segment3 = img[halfHeight:height, 0:halfWidth]
    segment4 = img[halfHeight:height, halfWidth:width]


    # Adding all the segments to a list of segments
    segments = [segment1, segment2, segment3, segment4]
    return segments

# Exercise 2
def isLightSegment(img):
    height = img.shape[0]
    width = img.shape[1]
    lightPixels = 0
    # Going through each pixel in the image
    # Height
    for y in range(height):
        # Width
        for x in range(width):

            # If the (b,g,r) value are being compared: [127, 127, 127]).all()
            # Comparing the pixels, (b,g,r) values by using the function .all(), if they are greater than (127,127,127),
            # than that pixel should be considered as a light pixel

            if (img[y, x] > [127, 127, 127]).all():
                lightPixels += 1

    # If the number of light pixels is more than half the total pixels in that segment (width* height of each segment)
    # Than that segment is considered as a light one
    if lightPixels >= (height * width) / 2:
        return True
    else:
        return False

# Exercise 3
def generateHistogram(img):
    # Generating the histogram using cv2.calcHist
    # Displaying the histogram using matplotlib
    hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    plt.plot(hist)
    plt.show()

# Exercise 4
def powerTransform(img, gamma):
    # c for 8 bit images is 255
    return np.array(255 * (img / 255) ** gamma, dtype='uint8')

# Exercise 5
def bitwiseSplicing(img):

    planes = []
    # Initializing an empty array with the size of the img inputted.
    # This is done for each plane.
    for i in range(8):
        planes.append(np.empty([img.shape[0],img.shape[1],img.shape[2]]))

    # Going through each pixel in each channel
    for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            for c in range(img.shape[2]):
                # Changing the pixel value to 8 bit binary
                binary = '{0:08b}'.format(img[y,x,c])
                # Going through each plane array (image) and changing its y,x coordinate value to the its respective
                # pixel value, where the first plane gets the last bit while the last plane (plane 8) get the first bit (binary [0])
                for i in range(len(planes)):
                    planes[i][y,x,c] = binary[7-i]
    return planes



