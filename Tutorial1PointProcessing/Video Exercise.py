import cv2
from Tutorial1PointProcessing.Methods import *

# Exercise 6
cap = cv2.VideoCapture("testVideo.mp4")
counter = 0
while(True):
    # Skipping 100 frames i.e 4 seconds considering 25 fps
    if counter % 100 != 0:
        ret, frame = cap.read()
        counter += 1
        continue
    else:
        ret, frame = cap.read()


    # Dividing the image into 4 segments
    segments = get4Segments(frame)

    # Going through each segment
    for segment in segments:
        # Checking if its a light segment
        if not isLightSegment(segment):
            # Displaying the dark segment
            cv2.imshow("Original Segment", segment)
            # Generating a histogram of the original segment
            generateHistogram(segment)
            # If it is, a dark segment
            # It is passed through a power transform with gamma 0.5
            # and its histogram is displayed
            lighterSegment = powerTransform(segment, 0.5)
            # Displaying the lightened segment
            cv2.imshow("Lightened Segment", lighterSegment)
            # Generating a histogram of the lightened segment
            generateHistogram(lighterSegment)
            cv2.waitKey(0)

    counter += 1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()