from Tutorial1PointProcessing.Methods import *

# Exercise 1

# Reading the image in grayscale
img = cv2.imread("../testImages/test.jpg")
img = cv2.resize(img,(960,600))


# Getting the 4 segments
segments = get4Segments(img)

"""
# Displaying each segment with the name of the quarter and the corresponding part of the image,
for i, segment in enumerate(segments):
    title =  str(i+1) + " segment"
    cv2.imshow(title, segment)

# Waiting for the user to press a key in order to continue the program.
cv2.waitKey(0)
"""

# Exercise 2
print("Checking for dark and light segments")

# Going through each segment
for i, segment in enumerate(segments):

    if isLightSegment(segment):
        print("Segment", i + 1, "is a light segment")
    else:
        print("Segment", i + 1, "is a dark segment")


    # Displaying the original Segment and generating a histrogram
    cv2.imshow("Segment", segment)
    generateHistogram(segment)

    # Displaying the power transformed image (gamma = 0.5) and its corresponding histogram
    lightenPowerTransformationSegment = powerTransform(segment,0.5)
    cv2.imshow("Lighter Power Transformed Segment" ,lightenPowerTransformationSegment)
    generateHistogram(lightenPowerTransformationSegment)

    # Displaying the power transformed image (gamma = 2) and its corresponding histogram
    darkenPowerTransformationSegment = powerTransform(segment, 2)
    cv2.imshow("Darker Power Transformed Segment", darkenPowerTransformationSegment)
    generateHistogram(darkenPowerTransformationSegment)
    cv2.waitKey(0)

    # Performing bitwise splicing on the segment
    planes = bitwiseSplicing(segment)

    # Going through each generated plane and displaying it
    for i,plane in enumerate(planes):
        cv2.imshow("Plane" + str(i+1), plane)
        cv2.waitKey(0)

    # Destroying all open windows to reduce clutter
    cv2.destroyAllWindows()







