# Code adapted from
# https://homepages.inf.ed.ac.uk/rbf/HIPR2/label.htm#:~:text=How%20It%20Works,set%20of%20intensity%20values%20V.
# https://homepages.inf.ed.ac.uk/rbf/HIPR2/dilate.htm
# https://homepages.inf.ed.ac.uk/rbf/HIPR2/erode.htm
import cv2
import numpy as np
from matplotlib import pyplot as plt

def getBinaryImage(image,thresholdLowerBound):
    # Converting image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Converting the image to a binary image
    binary = cv2.threshold(gray, thresholdLowerBound, 255, cv2.THRESH_BINARY_INV)[1]
    return binary

def connectedComponents(binary,connectivity):
    # Obtaining mask with connectComponents
    ret, labels = cv2.connectedComponents(binary, connectivity=connectivity)
    # Going through each label
    for label in range(1, ret):
        # Creating a new image for each label
        mask = np.array(labels, dtype=np.uint8)
        # noOfLabels = np.where(mask == label)
        # if len(noOfLabels[0]) < 10:
        #    continue
        # All the pixels with the current label are made white
        mask[labels == label] = 255
        cv2.imshow('component', mask)
        cv2.waitKey(0)

def customConnectedComponentsCore(binaryImage, connectivity=4):
    outputLabeledImg = np.zeros([binaryImage.shape[0],binaryImage.shape[1]],dtype=np.uint8)
    labelCounter = 1
    addLabelCounter = False

    for y in range(binaryImage.shape[0]):
        for x in range(binaryImage.shape[1]):
            if binaryImage[y,x] == 255:
                if connectivity == 4:
                    # Checking if the top, bottom, left, right pixels are white (4 neighbourhood)
                    if binaryImage[y-1,x] == 255 and binaryImage[y+1,x] == 255 and binaryImage[y,x+1] == 255 and binaryImage[y,x-1] == 255:
                        outputLabeledImg, labelCounter, addLabelCounter = connectedComponentCoreHelper(x, y, outputLabeledImg, labelCounter, addLabelCounter)
                    else:
                        # If the current pixel is not connected and a new label can be added, the label counter is incremented by 1
                        if addLabelCounter:
                            labelCounter += 1
                            addLabelCounter = False
                elif connectivity == 8:
                    # Checking if all the surrounding 8 pixels are white (8 pixel neighbourhood)
                    if binaryImage[y - 1, x] == 255 and binaryImage[y + 1, x] == 255 and binaryImage[y, x + 1] == 255 and \
                            binaryImage[y, x - 1] == 255 and binaryImage[y-1,x-1] == 255 and binaryImage[y-1,x+1] == 255 and binaryImage[y+1,x+1] == 255 and binaryImage[y+1,x-1] == 255:
                        outputLabeledImg, labelCounter,addLabelCounter = connectedComponentCoreHelper(x, y, outputLabeledImg, labelCounter, addLabelCounter)

                    else:
                        # If the current pixel is not connected and a new label can be added, the label counter is incremented by 1
                        if addLabelCounter:
                            labelCounter += 1
                            addLabelCounter = False
                else:
                    raise Exception("Incorrect connectivity")

    return outputLabeledImg, labelCounter

def connectedComponentCoreHelper(x, y, outputLabeledImg, labelCounter, addLabelCounter):
    possibleLabels = set()

    # Checking the surronding 8 pixels if any of them already have a label and therefore already part of a component which this pixel should be part of
    # A set is used to avoid repeated labels
    if outputLabeledImg[y - 1, x] != 0:
        possibleLabels.add(outputLabeledImg[y - 1, x])
    if outputLabeledImg[y + 1, x] != 0:
        possibleLabels.add(outputLabeledImg[y + 1, x])
    if outputLabeledImg[y, x + 1] != 0:
        possibleLabels.add(outputLabeledImg[y, x + 1])
    if outputLabeledImg[y, x - 1] != 0:
        possibleLabels.add(outputLabeledImg[y, x - 1])
    if outputLabeledImg[y + 1, x + 1] != 0:
        possibleLabels.add(outputLabeledImg[y + 1, x + 1])
    if outputLabeledImg[y - 1, x - 1] != 0:
        possibleLabels.add(outputLabeledImg[y - 1, x - 1])
    if outputLabeledImg[y + 1, x - 1] != 0:
        possibleLabels.add(outputLabeledImg[y + 1, x - 1])
    if outputLabeledImg[y - 1, x + 1] != 0:
        possibleLabels.add(outputLabeledImg[y - 1, x + 1])

    if len(possibleLabels) != 0:
        # If some of the surrounding pixels already have labels
        if len(possibleLabels) == 1:
            # If only 1 label is found than the current pixel is set to that label
            outputLabeledImg[y, x] = possibleLabels.pop()
        else:
            # If more than 1 label is found

            # The minimum label is found and the current pixel is set to this minimum label
            minLabel = min(possibleLabels)
            outputLabeledImg[y, x] = minLabel

            # All the labels found need to be changed to this minimum label since they are connected to the same component
            for label in possibleLabels:
                if label != minLabel:
                    outputLabeledImg[outputLabeledImg == label] = minLabel
                    """
                    # The labels that are to be changed are found on the current row one after the other going towards zero from the currentx position
                    for xTemp in range(x - 1, 0, -1):
                        if outputLabeledImg[y, xTemp] == label:
                            outputLabeledImg[y, xTemp] = minLabel
                        else:
                            # Since the labels required to be changed are contiguous, once the label is not found, the loop can stop
                            break
                    #labelCounter -= 1
                    """

        # The set is cleared for the next iteration
        possibleLabels.clear()

    else:
        # If no surrounding pixels already have labels, a new label is added to the current pixel
        outputLabeledImg[y, x] = labelCounter
        addLabelCounter = True

    return outputLabeledImg, labelCounter, addLabelCounter

def customConnectedComponents(binaryImage, connectivity):
    labeledImg, noOfLabels = customConnectedComponentsCore(binaryImage,connectivity)

    # Going through each label
    for label in range(1, noOfLabels):
        # Creating a new image for each label
        mask = np.array(labeledImg, dtype=np.uint8)
        # noOfLabels = np.where(mask == label)
        # if len(noOfLabels[0]) < 10:
        #    continue
        # All the pixels with the current label are made white
        mask[labeledImg == label] = 255
        cv2.imshow('component', mask)
        cv2.waitKey(0)


def extractROI(binary,origImage):
    # Using findContours to extract each ROI
    contours = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]
    for cnt in contours:
        # Getting the bound box where the ROI is
        (x, y, w, h) = cv2.boundingRect(cnt)
        # Displaying a new image with only the ROI
        ROI = origImage[y:y + h, x:x + w]
        cv2.imshow('ROI', ROI)
        cv2.waitKey(0)

def morphologicalTransformation(img, kernal, typeOfMorphology, iterations=1):
    # Generating an array filled with 0 with the same size of the input image
    output = np.zeros([img.shape[0], img.shape[1]], dtype='uint8')

    # Checking if input is correct
    if not (typeOfMorphology is "erode" or typeOfMorphology is "dilate"):
        print("Error in type of morphology")
        return 0

    for i in range(iterations):
        if i > 0:
            img = np.copy(output)
            output = np.zeros([img.shape[0], img.shape[1]], dtype='uint8')

        for y in range(img.shape[0]):
            for x in range(img.shape[1]):
                if img[y,x] == 255:
                    erodeCounter = 0
                    # Calculating the number of pixels to the side of the central pixel
                    halfSizeOfKernal = int((kernal.shape[0] - 1) / 2)

                    # Going through each vertical pixel of the kernel
                    for ykernel in range(kernal.shape[0]):
                        # Going through each horizontal pixel of the kernel
                        for xkernel in range(kernal.shape[1]):
                            # Assigning to the output pixel to the addition of all the values of the original pixel
                            # multiplied by the appropriate pixel of the kernel
                            # y + ykernal - halfSizeOfKernal and x + xkernal - halfSizeOfKernal are used since
                            # when the top left corner of the kernel is being used, the image y and x values
                            # have to go back
                            # Example for top left pixel ykernal will be 0 and halfSizeOfKernal will be the distance it
                            # has to go back

                            if y + ykernel - halfSizeOfKernal >= 0 and y + ykernel - halfSizeOfKernal <= img.shape[
                                0] - 1 and x + xkernel - halfSizeOfKernal >= 0 and x + xkernel - halfSizeOfKernal <= \
                                    img.shape[1] - 1:

                                if typeOfMorphology == "erode":
                                    # Multiplying the kernel by the image pixel and its neighbouring pixels to see if
                                    # they are all white
                                    erodeCounter += img[y + ykernel - halfSizeOfKernal, x + xkernel - halfSizeOfKernal] * kernal[ykernel, xkernel]

                                elif typeOfMorphology == "dilate":
                                    # Set all surrounding pixels to white
                                    output[y + ykernel - halfSizeOfKernal, x + xkernel - halfSizeOfKernal] = 255

                    # Only if the current pixel has all neighbouring pixels white, shall this pixel remain white in the output image
                    if typeOfMorphology == "erode" and erodeCounter == kernal.shape[0]*kernal.shape[1]*255:
                        output[y, x] = 255

    return output

def erode(img, kernel, iterations=1):
    img2 = np.copy(img)
    return morphologicalTransformation(img2,kernel,"erode",iterations)

def dilate(img, kernel, iterations=1):
    return morphologicalTransformation(img,kernel,"dilate",iterations)


def opening(img, kernel):
    output = erode(img,kernel,iterations=1)
    output = dilate(output,kernel,iterations=1)
    return output

def closing(img, kernel):
    output = dilate(img, kernel,iterations=1)
    output = erode(output, kernel, iterations=1)
    return output
