from Tutorial3Morphology.Methods3 import *

# Exercise 1
# Reading the image and converting it to binary
shapesImage = cv2.imread("images//shapes.jpg")
coinsImage = cv2.imread("images//Euro_Coins.jpg")

# lower bound = 240 for shapes.jpg
thresholdLowerBound = 240
binaryShapesImage = getBinaryImage(shapesImage,thresholdLowerBound)

cv2.imshow("Original Binary Shapes Image",binaryShapesImage)
cv2.waitKey(0)

# lower bound = 210 for Euro_Coins.jpg
thresholdLowerBound = 210
binaryCoinsImg = getBinaryImage(coinsImage,thresholdLowerBound)

cv2.imshow("Original Binary Coins Image",binaryCoinsImg)
cv2.waitKey(0)

# connected Components implemented from first principles
customConnectedComponents(binaryShapesImage,8)

# Previous implementation using open cv functions
#connectedComponents(binaryCoinsImg,8)

# Applying the findContours method
extractROI(binaryShapesImage,shapesImage)


cv2.destroyAllWindows()

# Exercise 2,3,4,5
# Reading image
textImg = cv2.imread("images//text.png")

# Getting binary image
# lower bound = 65 for text.png
thresholdLowerBound = 65
binaryTextImg = getBinaryImage(textImg,thresholdLowerBound)

# Displaying Orignal Image
cv2.imshow("Original Binary Image",binaryTextImg)
cv2.waitKey(0)

# Histogram of Original Image
hist = cv2.calcHist([binaryTextImg], [0], None, [256], [0, 256])
plt.plot(hist)
plt.show()

# Kernel for dilate and erode, opening and closing
kernel = np.ones((3,3),np.uint8)

# Applying dilate filter
dilatedImg = dilate(binaryTextImg, kernel,iterations=1)
cv2.imshow("Dilated image",dilatedImg)
cv2.waitKey(0)

# Histogram of Dilated Image
hist = cv2.calcHist([dilatedImg], [0], None, [256], [0, 256])
plt.plot(hist)
plt.show()


# Applying erode filter
erodedImg = erode(binaryTextImg, kernel,iterations=1)
cv2.imshow("Erode image",erodedImg)
cv2.waitKey(0)

# Histogram of eroded Image
hist = cv2.calcHist([erodedImg], [0], None, [256], [0, 256])
plt.plot(hist)
plt.show()


# Applying closing filter
closedImg = closing(binaryTextImg, kernel)
cv2.imshow("Closed image",closedImg)
cv2.waitKey(0)

# Histogram of closed Image
hist = cv2.calcHist([closedImg], [0], None, [256], [0, 256])
plt.plot(hist)
plt.show()

# Applying opening filter
opendedImg = opening(binaryTextImg, kernel)
cv2.imshow("Opened image",opendedImg)
cv2.waitKey(0)

# Histogram of opened Image
hist = cv2.calcHist([opendedImg], [0], None, [256], [0, 256])
plt.plot(hist)
plt.show()

cv2.destroyAllWindows()

# Exercise 6
# Applying closing filter
closedImg = closing(binaryTextImg, kernel)
cv2.imshow("Closed image",closedImg)
cv2.waitKey(0)

# Histogram of closed Image
hist = cv2.calcHist([closedImg], [0], None, [256], [0, 256])
plt.plot(hist)
plt.show()



noOfBlackPixelsToChange = 780
noblackPixels = 0
previousY = 0
nonText = True
text = False
counter = 0

# Going through each pixel in the closed image
for y in range(closedImg.shape[0]):
    noblackPixels = 0
    for x in range(closedImg.shape[1]):
        # If the pixel is black
        if closedImg[y,x] == 0:
            noblackPixels +=1


    # If the number of black pixels in this row is greater or equal to the value set above
    # and currently this is a text segment
    if noblackPixels >= noOfBlackPixelsToChange and text:
        # The nontext segment is now true
        nonText = True
        text = False
        # The text segment is displayed
        # From the previous y value where they was a change to the current y value and for all the width of the image
        ROI = binaryTextImg[previousY:y, 0:binaryTextImg.shape[1]]
        previousY = y
        counter += 1

        cv2.imshow('ROI', ROI)
        cv2.waitKey(0)
    # if the number of black pixels in this row is smaller than the value set above and current this is a non text segment
    elif noblackPixels < noOfBlackPixelsToChange and nonText:
        # The text segment is not true
        text = True
        nonText = False
        # The non text segment is displayed
        # From the previous y value where they was a change to the current y value and for all the width of the image
        ROI = binaryTextImg[previousY:y, 0:binaryTextImg.shape[1]]
        previousY = y
        counter += 1

        cv2.imshow('ROI', ROI)
        cv2.waitKey(0)


