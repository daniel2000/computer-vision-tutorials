import cv2
import numpy as np
from matplotlib import pyplot as plt

# Exercise 1
def slidingWindow(img, n, s, function, kernal):
    # Initializing an array the same size as the input image to store the output image
    output = np.zeros([img.shape[0], img.shape[1], img.shape[2]], dtype='uint8')


    # If the dimension of the window is not odd than the central pixel cannot be chosen
    # Therefore the dimension of the window is converted to odd
    if n % 2 == 0:
        print("n is not odd...reducing by 1")
        n -= 1

    # Calculating the number of pixels to the side of the central pixel of the window
    halfWidth = int((n - 1) / 2)

    # Calculating the number of pixels to the side of the central pixel of the kernel
    halfSizeOfKernel = int((kernal.shape[0] - 1) / 2)


    # Creating a new padded image with a 0 padding around the edge the same size as half of the kernel
    paddedRoi = np.zeros([img.shape[0] + 2 * halfSizeOfKernel, img.shape[1] + 2 * halfSizeOfKernel, img.shape[2]],dtype='uint8')

    for yTemp in range(paddedRoi.shape[0]):
        for xTemp in range(paddedRoi.shape[1]):
            if yTemp < halfSizeOfKernel or xTemp < halfSizeOfKernel or yTemp >= img.shape[
                0] + halfSizeOfKernel or xTemp >= img.shape[1] + halfSizeOfKernel:
                # Adding the actual padding
                paddedRoi[yTemp, xTemp] = 0
            else:
                # Adding the original image pixels to the inside of the padded image
                paddedRoi[yTemp, xTemp] = img[yTemp - halfSizeOfKernel, xTemp - halfSizeOfKernel]

    y = halfWidth
    # Going through each vertical pixel
    while y <= img.shape[0]:
        x = halfWidth
        # Going through each horizontal pixel
        while x <= img.shape[1]:

            # If another window doesn't fit the x and y values are adjusted so the window will fit exactly with end of the image
            # This is to make sure that the whole image is convolved with the kernel/window
            if y + halfWidth > img.shape[0]:
                y = img.shape[0] - halfWidth
            if x + halfWidth > img.shape[1]:
                x = img.shape[1] - halfWidth


            # Checking if all the window will lie inside of the image and not overflow out of it
            if x - halfWidth >= 0 and x + halfWidth <= img.shape[1] and y - halfWidth >= 0 and y + halfWidth <= \
                    img.shape[0]:

                # Calling the convolution function on the desired window with the desired kernel
                # The desired window in from y - halfWidth (half width of the window) to y+ halfWidth
                # and x -halfWidth to x+halfWidth
                # The half size of the kernel is added to all the above to shift each value by the number of padding added to the side
                convolvedImage = function(paddedRoi, y - halfWidth+halfSizeOfKernel,y + halfWidth+halfSizeOfKernel,x - halfWidth+halfSizeOfKernel,x + halfWidth+halfSizeOfKernel, kernal)


                # Initializing a part of the output image with the convolved part of the image
                output[y - halfWidth:y + halfWidth, x - halfWidth:x + halfWidth] = convolvedImage

                # When displaying the rectangle
                # Creating a rectangle along with given the extreme pixels of the slider
                """
                rectangle = cv2.rectangle(img, (x - halfWidth, y - halfWidth), (x + halfWidth, y + halfWidth),(255, 0, 0), 2)
                cv2.imshow("Img", rectangle)
                cv2.waitKey(0)
                """

            # Incrementing the x value by the stride value, since each stride should be s apart
            x += s
        # Incrementing the y value by the stride value, since each stride should be s apart
        y += s
    return output


def convolutionOnROI(roi, minY, maxY, minX, maxX, kernel):
    # Generating an array filled with 0 with the same size of the input image
    output = np.zeros([maxY - minY, maxX - minX, roi.shape[2]], dtype='uint8')

    # Calculating the number of pixels to the side of the central pixel
    halfSizeOfKernal = int((kernel.shape[0] - 1) / 2)

    # Going through each vertical pixel
    for y in range(minY,maxY):
        # Going through each horizontal pixel
        for x in range(minX,maxX):
                # Going through each colour channel
                for c in range(roi.shape[2]):
                    # Going through each vertical pixel of the kernel
                    for ykernel in range(kernel.shape[0]):
                        # Going through each horizontal pixel of the kernel
                        for xkernel in range(kernel.shape[1]):
                            # Assigning to the output pixel to the addition of all the values of the original pixel
                            # multiplied by the appropriate pixel of the kernel

                            # y-minY and x-minX are used to assign the output pixels since the output image should
                            # start from 0,0 while the x and y variables start from minY and minX.
                            # Therefore this displacement is removed


                            # y + ykernal - halfSizeOfKernal and x + xkernal - halfSizeOfKernal are used since
                            # when the top left corner of the kernel is being used, the image y and x values
                            # have to go back

                            # Example for top left pixel ykernal will be 0 and halfSizeOfKernal will be the distance it
                            # has to go back
                            output[y-minY, x-minX, c] += roi[y + ykernel - halfSizeOfKernal, x + xkernel - halfSizeOfKernal, c] * kernel[ykernel, xkernel]

    return output


def convolutionOnImage(image,n,stride, kernal):
    # Displaying the original Image
    cv2.imshow("Original Image", image)
    cv2.waitKey(0)

    print("Convolving image with filter")

    # Using the sliding window function to go over each window and applying the kernel to each window by going
    # through the function convolutionOnROI
    output = slidingWindow(image, n, stride, convolutionOnROI, kernal)
    cv2.imshow("Convolved  Image", output)
    cv2.waitKey(0)

    # Histogram of Original Image
    hist = cv2.calcHist([image], [0], None, [256], [0, 256])
    plt.plot(hist)
    plt.show()

    # Histogram of Convolved Image
    hist2 = cv2.calcHist([output], [0], None, [256], [0, 256])
    plt.plot(hist2)
    plt.show()


def generateGaussianKernal(k, sigma):
    # The kernal size has to be odd in order to accomodate for the central pixel
    if k % 2 == 0:
        print("Reducing size of kernal by 1")
        k -= 1

    # Generating an empty array of the appropriate size (k x k)
    kernal = np.zeros([k, k])

    # Calculating the number of pixels to the side of the central pixel
    sideSizeOfKernal = int((k - 1) / 2)

    # Going from the far left to the far right of the central, it the central pixel is (0,0)
    for y in range(-sideSizeOfKernal, sideSizeOfKernal + 1):
        for x in range(-sideSizeOfKernal, sideSizeOfKernal + 1):
            # Initializing each element of the kernel array with the calculated value
            # y+sideSizeOfKernal and x+sideSizeOfKernal are used since the (0,0) is at the top left corner
            kernal[y + sideSizeOfKernal, x + sideSizeOfKernal] = (1 / (2 * np.pi * (sigma ** 2))) * (
                        np.e ** (-(y ** 2 + x ** 2) / (2 * sigma ** 2)))

    return kernal

def convertToGrayscale(img):
    # Converting to grayscale by adding another array to each element
    output = np.zeros([img.shape[0], img.shape[1], 1], dtype='uint8')
    for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            output[y, x, 0] = img[y, x]
    return output


# Reading the image and resizing it
img = cv2.imread("../testImages/test.jpg")
img = cv2.resize(img, (422, 209))

# Reading the image and resizing it
imgGray = cv2.imread("../testImages/test.jpg",0)
imgGray = cv2.resize(imgGray, (422, 209))

# When testing out sobel, the below method should be used and the above imread second parameter should be set to 0
# Used to 'convert' an image to grayscale
imgGray = convertToGrayscale(imgGray)


# Kernel Generation
sobel = (1 / 8) * np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype='float64')
bilinear = (1 / 16) * np.array([[1, 2, 1], [2, 4, 2], [1, 2, 1]], dtype='float64')
gaussian = generateGaussianKernal(5, 1)

print("Convolving with Sobel")
print("Please press enter after seeing original image")
# Convolving the sobel kernel over the whole image segment by segment
convolutionOnImage(imgGray,100,50, sobel)
print("-------------------")
cv2.destroyAllWindows()

print("Convolving with Bilinear")
print("Please press enter after seeing original image")
# Convolving the bilinear kernel over the whole image segment by segment
convolutionOnImage(img,100,50, bilinear)
print("-------------------")
cv2.destroyAllWindows()

print("Convolving with Gaussian")
print("Please press enter after seeing original image")
# Convolving the gaussian kernel over the whole image segment by segment
convolutionOnImage(img,100,50, gaussian)
print("-------------------")
cv2.destroyAllWindows()

