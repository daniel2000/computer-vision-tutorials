import cv2
import numpy as np
from matplotlib import pyplot as plt

# Exercise 1
def cornerHarris(img):
    # Converting the image to grayscale
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)

    # Parameters for corner Harris
    blocksize =2

    # ksize odd number between 1 and 31
    ksize =1
    # k between 0.04 - 0.06
    k = 0.06

    dst = cv2.cornerHarris(gray,blocksize,ksize,k)

    # Result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)

    # Threshold for an optimal value, it may vary depending on the image.
    # Varying the threshold value will vary the number of features shown
    img[dst>0.015*dst.max()]=[0,0,255]


    cv2.imshow('Corner Harris',img)
    cv2.waitKey(0)


def shiTomasiCornerDetector(img):
    # Coverting the image to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # No of corners
    noOfCorners = 1000
    # Quality level between 0 and 1
    qualityLevel = .2
    # Minimum euclidean distance between corners detected
    minEuclideanDistance = 1


    # Performing the shi tomasi through the function good features to track
    corners = cv2.goodFeaturesToTrack(gray, noOfCorners, qualityLevel, minEuclideanDistance)
    corners = np.int0(corners)

    for i in corners:
        x, y = i.ravel()
        cv2.circle(img, (x, y), 3, 255, -1)

    # Converting the image to RGB which matplotlib uses
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


    plt.imshow(img), plt.show()


def SIFT(img):
    # Converting the image to gray
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Creating the sift object, detecting and drawing the key points on the image
    sift = cv2.xfeatures2d.SIFT_create()
    kp,dest = sift.detectAndCompute(gray, None)
    cv2.drawKeypoints(gray, kp,img)

    # Displaying the image
    cv2.imshow("SIFT Key points", img)
    cv2.waitKey(0)

    return (kp, dest)


def SURF(img):
    # Converting the image to gray
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Hessian Threshold
    hessianThreshold = 950

    # Initializing a SURF Object
    surf = cv2.xfeatures2d.SURF_create(hessianThreshold)

    # Detecting and computing the features
    kp, dest = surf.detectAndCompute(gray, None)
    # Marking the features detected on the img
    cv2.drawKeypoints(gray,kp,img)

    # Displaying the image
    cv2.imshow("SURF Key points", img)
    cv2.waitKey(0)

    return (kp, dest)


def ORB(img):
    # Initializing an ORB object
    orb = cv2.ORB_create(5000)

    # Find the keypoints with ORB
    kp = orb.detect(img, None)

    # Compute the descriptors
    kp, des = orb.compute(img, kp)

    # Drawing the features on the output image
    cv2.drawKeypoints(img, kp, img)

    # Displaying the image
    cv2.imshow("ORB Keypoints", img)
    cv2.waitKey(0)

    return (kp,des)

def BFMatcher(img1, img2, detector):
    # Resizing the images and keeping the same aspect ratio
    img1 = cv2.resize(img1,(1000,563))
    img2 = cv2.resize(img2,(1000,563))

    if detector == "sift":
        # Obtaining the keypoints and descriptors for the SIFT feature detector for both images
        ret1 = SIFT(img1)
        ret2 = SIFT(img2)
        normType = cv2.NORM_L2
    elif detector == "surf":
        # Obtaining the keypoints and descriptors for the SURF feature detector for both images
        ret1 = SURF(img1)
        ret2 = SURF(img2)
        normType = cv2.NORM_L2
    elif detector == "orb":
        # Obtaining the keypoints and descriptors for the ORB feature detector for both images
        ret1 = ORB(img1)
        ret2 = ORB(img2)
        # The normType in BFMatcher is set to NORM_HAMMERING for the ORB feature detector
        normType = cv2.NORM_HAMMING
    else:
        print("Enter valid feature detector")
        return 0

    # Initialize a BF matcher object
    bf = cv2.BFMatcher(normType)

    # Match descriptors
    matches = bf.match(ret1[1], ret2[1])

    # Sort them in the order of their distance.
    matches = sorted(matches, key=lambda x: x.distance)

    # Output image
    img3 = np.array((img1.shape[0]+img2.shape[0],img1.shape[1]+img2.shape[1]),dtype=np.uint8)

    # Drawing the matching feature detectors
    img3 = cv2.drawMatches(img1, ret1[0], img2, ret2[0], matches[:100],img3, flags=2)

    cv2.imshow("BF Matcher", img3)
    cv2.waitKey(0)



filename = '3_colour.jpeg'
img = cv2.imread(filename)

# Performing the corner harris corner detector
cornerHarris(img)

# Obtaining the original picture
img = cv2.imread(filename)

# Performing the Shi Tomasi corner detector
shiTomasiCornerDetector(img)
print("Shi Tomasi Output using plt plot")

# Obtaining the original picture
img = cv2.imread(filename)

# Performing the SIFT feature detector
SIFT(img)

# Obtaining the original picture
img = cv2.imread(filename)

# Performing the SURF feature detector
SURF(img)

# Obtaining the original picture
img = cv2.imread(filename)

# Performing the ORB feature detector
ORB(img)

cv2.destroyAllWindows()

# Obtaining the original picture
img = cv2.imread(filename)

filename = '2_colour.jpeg'
img2 = cv2.imread(filename)

# Performing BFMatcher
print("BF Matcher")
# Choose from orb, sift or surf
BFMatcher(img,img2,"orb")
